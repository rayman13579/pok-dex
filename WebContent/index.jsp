<%@page import="at.ray.web.pokemon.Pokemon"%>
<%@page import="at.ray.web.pokemon.PokemonDBDAO"%>
<%@page import="java.util.List"%>
<%@page import="org.eclipse.jdt.internal.compiler.ast.WhileStatement"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="./res/css/bootstrap.css" rel="stylesheet" />
<script src="./res/js/jquery.js"></script>
<title>Pok� Wiki</title>
</head>
<body background="res/images/grass.png" id="body"
	style="width: 99%; font-family: Pokemon Hollow; color: #365C9F;">
	<script>
		$(document).ready(
				function() {
					$('#search').keyup(
							function() {
								if (event.keyCode == 13) {
									var searchfor = $("input[name='search']")
											.val();
									window.open(
											"http://localhost:8080/01_Basics/index.jsp?search="
													+ searchfor, "_self")
								}
							});
				});
	</script>
	<div class="row"></div>
	<div class="row">
		<div class="col-md-1">
			<button class="btn" style="background-color: transparent;"
				onclick="window.open('http://localhost:8080/01_Basics/index.jsp' ,'_self')">
				<p style="font-size: 30pt">< Back</p>
			</button>
		</div>
		<div class="col-md-10">
			<div style="background: transparent;" class="jumbotron">
				<h1>Benjamin and Roman's Pok�mon Wiki!!!</h1>
				</br>
				<p>The Best Wiki in all Universes! \O/</p>
				<div class="row">
					<div class="col-lg-6">
						<div class="form-group" style="width: 500px;">
							<input id="search" name="search" type="text"
								class="form-control input-lg"
								placeholder="Walk into tall grass...">
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-1"></div>
	</div>

	<div class="row">
		<div class="col-md-1"></div>
		<div class="col-md-10">
			<h2>
				<b>Choose your Starter!</b>
			</h2>
			<br>
			<div class="row">
				<%
					PokemonDBDAO dao = new PokemonDBDAO();
					String search = request.getParameter("search");
					List<Pokemon> pokemonList;
					if (search != null) {
						pokemonList = dao.searchPokemon(search);
					} else {
						pokemonList = dao.getAllPokemon();
					}

					for (Pokemon pokemon : pokemonList) {
				%>
				<div class="col-md-4">
					<p>
						<a href="detail.jsp?id=<%=pokemon.getId()%>"><button
								type="button"
								style="min-width: 275px; background-color:<%=pokemon.getColour()%>; border-style: none; color: black;"
								class="btn-lg text-middle">
								<span class="glyphicon glyphicon-<%=pokemon.getSymbol()%>"
									aria-hidden="true"></span>
								<%=pokemon.getName()%>
							</button></a>
					</p>
				</div>

				<%
					}
				%>

			</div>
			<div class="col-md-1"></div>
		</div>
	</div>
</body>
</html>