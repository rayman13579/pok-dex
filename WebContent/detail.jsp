<%@page import="at.ray.web.pokemon.Pokemon"%>
<%@page import="at.ray.web.pokemon.PokemonDBDAO"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link href="./res/css/bootstrap.css" rel="stylesheet" />
<link href=' http://fonts.googleapis.com/css?family=Droid+Sans'
	rel='stylesheet' type='text/css'>
<script src="./res/js/jquery.js"></script>
<%
	String id = request.getParameter("id");
	PokemonDBDAO dao = new PokemonDBDAO();

	int idi = Integer.parseInt(id);
	Pokemon pokemon = dao.getPokemonById(idi - 1);
%>

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Details of <%=pokemon.getName()%></title>
</head>
<body style="width: 99%; font-family: Pokemon Hollow; color: #365C9F;">
	<img src="./res/images/pokemon/<%=pokemon.getName()%>.jpg"
		style="position: absolute; top: 0px; left: 0px; width: 100%; max-height: 100%; z-index: -10;">
	<div class="row">
		<div class="col-md-1">
			<button class="btn" style="background-color: transparent;"
				onclick="window.history.back();">
				<p style="font-size: 30pt">< Back</p>
			</button>
		</div>
		<div class="col-md-5">
			<h1 style="text-align: right; font-size: 80px;"><%=pokemon.getName()%></h1>
		</div>
	</div>
	<%
		for (int x = 0; x <= 4; x++) {
	%>
	<br>
	<%
		}
	%>
	<div class="row">
		<%
			if (pokemon.getId() == 5) {
		%>
		<div class="col-md-7">
			<%
				} else {
			%>
			<div class="col-md-3">
				<%
					}
				%>
				<div class="container">
					<table class="table" style="width: 20px; font-size: 30px;">
						<thead>
							<th colspan="2" style="text-align: center">Attributes</th>
						</thead>
						<tr>
							<td>Size</td>
							<td><%=pokemon.getSize()%>m</td>
						</tr>
						<tr>
							<td>Weight</td>
							<td><%=pokemon.getWeight()%>kg</td>
						</tr>
						<tr>
							<td>Sex</td>
							<td><%=pokemon.getSex()%></td>
						</tr>
						<tr>
							<td>Type</td>
							<td><%=pokemon.getType()%></td>
						</tr>
						</tbody>
					</table>
				</div>
			</div>
			<div class="col-md-4">
				<%
					if (pokemon.getId() == 5) {
				%>
				<%
					for (int x = 0; x <= 16; x++) {
				%>
				<br>
				<%
					}
				%>
				<%
					}
				%>
				<p style="font-size: 22pt;"><%=pokemon.getStory()%></p>
			</div>
		</div>
</body>

</html>