package at.ray.web.pokemon;

public class Pokemon {
	private int id;
	private String name;
	private float size;
	private float weight;
	private String sex;
	private String fk_type;
	private String type;
	private String colour;
	private String symbol;
	private String story;

	public Pokemon(int id, String name, float size, float weight, String sex, String fk_type, String type,
			String colour, String symbol, String story) {
		super();
		this.id = id;
		this.name = name;
		this.size = size;
		this.weight = weight;
		this.sex = sex;
		this.fk_type = fk_type;
		this.type = type;
		this.colour = colour;
		this.symbol = symbol;
		this.story = story;
	}

	public int getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public float getSize() {
		return size;
	}

	public float getWeight() {
		return weight;
	}

	public String getSex() {
		return sex;
	}

	public String getFk_type() {
		return fk_type;
	}

	public String getType() {
		return type;
	}

	public String getColour() {
		return colour;
	}

	public String getSymbol() {
		return symbol;
	}

	public String getStory() {
		return story;
	}
}