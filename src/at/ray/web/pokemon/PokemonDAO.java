package at.ray.web.pokemon;

import java.util.List;
import java.util.ArrayList;

public class PokemonDAO {
	private List<Pokemon> pokemonList;

	public PokemonDAO() {
		this.pokemonList = new ArrayList<>();
//		this.pokemonList.add(new Pokemon(0, "Charmander", "Fire", "danger", "fire"));
//		this.pokemonList.add(new Pokemon(1, "Squirtle", "Water", "info", "tint"));
//		this.pokemonList.add(new Pokemon(2, "Bulbasur", "Grass", "success", "leaf"));
//		this.pokemonList.add(new Pokemon(3, "Cyndaquil", "Fire", "danger", "fire"));
//		this.pokemonList.add(new Pokemon(4, "Totodile", "Water", "info", "tint"));
//		this.pokemonList.add(new Pokemon(5, "Chikorita", "Grass", "success", "leaf"));
//		this.pokemonList.add(new Pokemon(6, "Torchic", "Fire", "danger", "fire");
//		this.pokemonList.add(new Pokemon(7, "Mudkip", "Water", "info", "tint"));
//		this.pokemonList.add(new Pokemon(8, "Treecko", "Grass", "success", "leaf"));
	}

	public List<Pokemon> getAllPokemon() {
		return this.pokemonList;
	}

	public Pokemon getPokemonById(int id) {
		return this.pokemonList.get(id);
	}
}