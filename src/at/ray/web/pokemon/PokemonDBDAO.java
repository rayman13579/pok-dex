package at.ray.web.pokemon;

import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import java.sql.Connection;

public class PokemonDBDAO {
	public Connection connection;
	List<Pokemon> pokemonList = new ArrayList<>();

	public PokemonDBDAO() {
		try {
			Class.forName("com.mysql.jdbc.Driver");

			String URL = "jdbc:mysql://localhost/pokedex";
			String USER = "root";
			String PASS = "";
			this.connection = DriverManager.getConnection(URL, USER, PASS);
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
			System.out.println("Error");
		}
	}

	public List<Pokemon> getAllPokemon() {
		try {
			Statement st = this.connection.createStatement();
			ResultSet rs = st.executeQuery("Select * from pokemon p inner join type t on p.fk_type=t.type order by id");
			rs.first();

			while (!rs.isAfterLast()) {
				Pokemon p = new Pokemon(rs.getInt("id"), rs.getString("name"), rs.getFloat("size"),
						rs.getFloat("weight"), rs.getString("sex"), rs.getString("fk_type"), rs.getString("type"),
						rs.getString("colour"), rs.getString("symbol"), rs.getString("story"));
				pokemonList.add(p);
				rs.next();
			}
			st.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return pokemonList;
	}

	public Pokemon getPokemonById(int id) {
		try {
			Statement st = this.connection.createStatement();
			ResultSet rs = st.executeQuery("Select * from pokemon inner join type on pokemon.fk_type=type.type");
			rs.first();

			while (!rs.isAfterLast()) {
				Pokemon p = new Pokemon(rs.getInt("id"), rs.getString("name"), rs.getFloat("size"),
						rs.getFloat("weight"), rs.getString("sex"), rs.getString("fk_type"), rs.getString("type"),
						rs.getString("colour"), rs.getString("symbol"), rs.getString("story"));
				pokemonList.add(p);
				rs.next();
			}
			st.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return this.pokemonList.get(id);
	}

	public List<Pokemon> searchPokemon(String search) {
		try {
			Statement st = this.connection.createStatement();
			ResultSet rs = st.executeQuery(
					"Select * from pokemon inner join type on pokemon.fk_type=type.type where pokemon.name like '%"
							+ search + "%' order by pokemon.name");
			rs.first();

			while (!rs.isAfterLast()) {
				Pokemon p = new Pokemon(rs.getInt("id"), rs.getString("name"), rs.getFloat("size"),
						rs.getFloat("weight"), rs.getString("sex"), rs.getString("fk_type"), rs.getString("type"),
						rs.getString("colour"), rs.getString("symbol"), rs.getString("story"));
				pokemonList.add(p);
				rs.next();
			}
			st.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return pokemonList;
	}

	public static void main(String[] args) {
		PokemonDBDAO dao = new PokemonDBDAO();
		List<Pokemon> pList = dao.getAllPokemon();
		for (Pokemon pokemon : pList) {
			System.out.println(pokemon.getName());
		}
	}

}
